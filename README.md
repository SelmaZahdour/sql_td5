# sql_td5

## Partie 1

-- Nombre de patients ont été hospitalisés, au global et par années. test


-- Nombre de patients décédés avant la sortie (mode de sortie = 9) ainsi que l'âge moyen au décès.


-- Nombre de RUM par UF et la durée moyenne de passage dans les UF (avec date_entree et date_sortie)


-- Distribution des diagnostics principaux, diagnostics secondaires et des actes

## Partie 2

-- Combien d'UF différentes sont traversées au cours d'un séjour ? Med, Q1, Q3, min, max

-- Quelles sont les unités traversées en premier ?

-- Quelles sont les unités traversées en dernier ?

-- Dans le sous-groupe de séjours avec un seul RUM : descriptions des UFs, modes d'entrée, modes de sortie, et durée du RUM. 

-- Comparaison des patients décédés et viviants : un seul rum ou plusieurs rums ?

-- Compter les associations d'UFs pour les deux premiers RUMs.

-- Compter les associations d'UFs pour toutes les transitions entre RUMs.

-- Compter les transitions du mode d'entrée vers le premier RUM.

-- Compter les transitions du dernier rum vers le mode de sortie.

-- Compter les séquences complètes (ex : Urgences - UF1 - UF2 - Domicile = 5 séjours).